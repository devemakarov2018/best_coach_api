<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TheoryDetailsComponentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TheoryDetailsComponentsTable Test Case
 */
class TheoryDetailsComponentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TheoryDetailsComponentsTable
     */
    public $TheoryDetailsComponents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.theory_details_components',
        'app.theory_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TheoryDetailsComponents') ? [] : ['className' => TheoryDetailsComponentsTable::class];
        $this->TheoryDetailsComponents = TableRegistry::getTableLocator()->get('TheoryDetailsComponents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TheoryDetailsComponents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
