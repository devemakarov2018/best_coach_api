<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TheoryDetailsCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TheoryDetailsCategoriesTable Test Case
 */
class TheoryDetailsCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TheoryDetailsCategoriesTable
     */
    public $TheoryDetailsCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.theory_details_categories',
        'app.theory_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TheoryDetailsCategories') ? [] : ['className' => TheoryDetailsCategoriesTable::class];
        $this->TheoryDetailsCategories = TableRegistry::getTableLocator()->get('TheoryDetailsCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TheoryDetailsCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
