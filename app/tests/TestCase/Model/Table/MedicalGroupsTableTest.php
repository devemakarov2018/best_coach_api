<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MedicalGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MedicalGroupsTable Test Case
 */
class MedicalGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MedicalGroupsTable
     */
    public $MedicalGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.medical_groups',
        'app.students'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MedicalGroups') ? [] : ['className' => MedicalGroupsTable::class];
        $this->MedicalGroups = TableRegistry::getTableLocator()->get('MedicalGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MedicalGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
