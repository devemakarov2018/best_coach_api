<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TheoryDetailsTheoryDetailsCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TheoryDetailsTheoryDetailsCategoriesTable Test Case
 */
class TheoryDetailsTheoryDetailsCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TheoryDetailsTheoryDetailsCategoriesTable
     */
    public $TheoryDetailsTheoryDetailsCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.theory_details_theory_details_categories',
        'app.theory_details',
        'app.theory_details_categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TheoryDetailsTheoryDetailsCategories') ? [] : ['className' => TheoryDetailsTheoryDetailsCategoriesTable::class];
        $this->TheoryDetailsTheoryDetailsCategories = TableRegistry::getTableLocator()->get('TheoryDetailsTheoryDetailsCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TheoryDetailsTheoryDetailsCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
