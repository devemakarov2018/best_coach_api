<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TheoryDetailsStudentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TheoryDetailsStudentsTable Test Case
 */
class TheoryDetailsStudentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TheoryDetailsStudentsTable
     */
    public $TheoryDetailsStudents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.theory_details_students',
        'app.theory_details',
        'app.students'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TheoryDetailsStudents') ? [] : ['className' => TheoryDetailsStudentsTable::class];
        $this->TheoryDetailsStudents = TableRegistry::getTableLocator()->get('TheoryDetailsStudents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TheoryDetailsStudents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
