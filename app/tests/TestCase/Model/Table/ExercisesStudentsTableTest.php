<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExercisesStudentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExercisesStudentsTable Test Case
 */
class ExercisesStudentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExercisesStudentsTable
     */
    public $ExercisesStudents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exercises_students',
        'app.exercises',
        'app.students'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ExercisesStudents') ? [] : ['className' => ExercisesStudentsTable::class];
        $this->ExercisesStudents = TableRegistry::getTableLocator()->get('ExercisesStudents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExercisesStudents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    public function testCheckSchedule()
    {
        //1) interval_mode == day

        //1.1) end_mode == date

        //1.1.1) date > end_date
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 1,
                    'end_mode' => 'date',
                    'end_date' => '2018-11-03'
                ]),
                new \DateTime('2018-11-04')
            )
        );

        //1.1.2) date diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 2,
                    'end_mode' => 'date',
                    'end_date' => '2018-12-01'
                ]),
                new \DateTime('2018-11-02')
            )
        );

        //1.1.3) date diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 2,
                    'end_mode' => 'date',
                    'end_date' => '2018-12-01'
                ]),
                new \DateTime('2018-11-03')
            )
        );

        //1.1.4) date == end_date
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 1,
                    'end_mode' => 'date',
                    'end_date' => '2018-11-03'
                ]),
                new \DateTime('2018-11-03')
            )
        );

        //1.2) end_mode == repeats

        //1.2.1) used all tries
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 1,
                    'end_mode' => 'repeats',
                    'end_repeats' => 2,
                    'repeats' => [
                        ['date' => '2018-11-02', 'success' => true, 'fail' => false],
                        ['date' => '2018-11-03', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2018-11-04')
            )
        );

        //1.2.2) enough tries but date diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 5,
                    'end_mode' => 'repeats',
                    'end_repeats' => 10,
                    'repeats' => [
                        ['date' => '2018-11-02', 'success' => true, 'fail' => false],
                        ['date' => '2018-11-03', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2018-11-07')
            )
        );

        //1.2.3) enough tries and date diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 5,
                    'end_mode' => 'repeats',
                    'end_repeats' => 10,
                    'repeats' => [
                        ['date' => '2018-11-02', 'success' => true, 'fail' => false],
                        ['date' => '2018-11-03', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2018-11-11')
            )
        );

        //1.3) end_mode == endless

        //1.3.1) date diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 5,
                    'end_mode' => 'endless',
                ]),
                new \DateTime('2018-11-07')
            )
        );

        //1.3.2) date diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'day',
                    'interval' => 5,
                    'end_mode' => 'endless',
                ]),
                new \DateTime('2018-11-06')
            )
        );

        //2) interval_mode == month

        //2.1) end_mode == date

        //2.1.1) date > end_date
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 1,
                    'end_mode' => 'date',
                    'end_date' => '2018-11-30'
                ]),
                new \DateTime('2018-12-01')
            )
        );

        //2.1.2) date diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 1,
                    'end_mode' => 'date',
                    'end_date' => '2019-11-01'
                ]),
                new \DateTime('2018-12-02')
            )
        );

        //2.1.3) date diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-03',
                    'interval_mode' => 'month',
                    'interval' => 2,
                    'end_mode' => 'date',
                    'end_date' => '2019-11-01'
                ]),
                new \DateTime('2019-01-03')
            )
        );

        //2.2) end_mode == repeats

        //2.2.1) used all tries
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 1,
                    'end_mode' => 'repeats',
                    'end_repeats' => 2,
                    'repeats' => [
                        ['date' => '2018-12-01', 'success' => true, 'fail' => false],
                        ['date' => '2019-01-01', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2019-02-01')
            )
        );

        //2.2.2) enough tries but date diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 1,
                    'end_mode' => 'repeats',
                    'end_repeats' => 10,
                    'repeats' => [
                        ['date' => '2018-12-01', 'success' => true, 'fail' => false],
                        ['date' => '2019-01-01', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2019-02-03')
            )
        );

        //2.2.3) enough tries and date diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 1,
                    'end_mode' => 'repeats',
                    'end_repeats' => 10,
                    'repeats' => [
                        ['date' => '2018-12-01', 'success' => true, 'fail' => false],
                        ['date' => '2019-01-01', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2019-03-01')
            )
        );

        //2.3) end_mode == endless

        //2.3.1) date diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 3,
                    'end_mode' => 'endless',
                ]),
                new \DateTime('2019-03-01')
            )
        );

        //2.3.2) date diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'month',
                    'interval' => 2,
                    'end_mode' => 'endless',
                ]),
                new \DateTime('2019-03-01')
            )
        );

        //3) interval_mode == week

        //3.1) end_mode == date

        //3.1.1) date > end_date
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 1,
                    'end_mode' => 'date',
                    'end_date' => '2018-11-30',
                    'days_of_week' => [
                        'Mon' => true,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => false,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false
                    ]
                ]),
                new \DateTime('2018-12-01')
            )
        );

        //3.1.2) week diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 2,
                    'end_mode' => 'date',
                    'end_date' => '2018-11-30',
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => true,
                        'Thu' => false,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false
                    ]
                ]),
                new \DateTime('2018-11-07')
            )
        );

        //3.1.2) week diff % interval == 0 but days of week don't same
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 3,
                    'end_mode' => 'date',
                    'end_date' => '2018-10-30',
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => true,
                        'Thu' => false,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false
                    ]
                ]),
                new \DateTime('2018-11-24')
            )
        );

        //3.1.3) week diff % interval == 0 and same days of week
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 3,
                    'end_mode' => 'date',
                    'end_date' => '2018-11-30',
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => true,
                        'Thu' => false,
                        'Fri' => false,
                        'Sat' => true,
                        'Sun' => false
                    ]
                ]),
                new \DateTime('2018-11-24')
            )
        );

        //3.1.4) week diff % interval == 0 and same days of week but different years
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-12-01',
                    'interval_mode' => 'week',
                    'interval' => 2,
                    'end_mode' => 'date',
                    'end_date' => '2019-01-30',
                    'days_of_week' => [
                        'Mon' => true,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => false,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false,
                    ]
                ]),
                new \DateTime('2019-01-07')
            )
        );

        //3.2) end_mode == repeats

        //3.2.1) used all tries
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 1,
                    'end_mode' => 'repeats',
                    'end_repeats' => 2,
                    'repeats' => [
                        ['date' => '2018-11-04', 'success' => true, 'fail' => false],
                        ['date' => '2018-11-11', 'success' => true, 'fail' => false],
                    ],
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => false,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => true,
                    ]
                ]),
                new \DateTime('2018-11-18')
            )
        );

        //3.2.2) enough tries but week diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 2,
                    'end_mode' => 'repeats',
                    'end_repeats' => 10,
                    'repeats' => [
                        ['date' => '2018-12-01', 'success' => true, 'fail' => false],
                        ['date' => '2018-01-01', 'success' => true, 'fail' => false],
                    ],
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => true,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false,
                    ]
                ]),
                new \DateTime('2018-11-22')
            )
        );

        //3.2.3) enough tries and week diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 2,
                    'end_mode' => 'repeats',
                    'end_repeats' => 10,
                    'repeats' => [
                        ['date' => '2018-12-01', 'success' => true, 'fail' => false],
                        ['date' => '2018-01-01', 'success' => true, 'fail' => false],
                    ],
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => true,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false,
                    ]
                ]),
                new \DateTime('2018-11-29')
            )
        );

        //3.3) end_mode == endless

        //3.3.1) week diff % interval != 0
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 2,
                    'end_mode' => 'endless',
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => true,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false,
                    ]
                ]),
                new \DateTime('2018-11-22')
            )
        );

        //3.3.2) week diff % interval == 0
        $this->assertTrue(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-11-01',
                    'interval_mode' => 'week',
                    'interval' => 2,
                    'end_mode' => 'endless',
                    'days_of_week' => [
                        'Mon' => false,
                        'Tue' => false,
                        'Wed' => false,
                        'Thu' => true,
                        'Fri' => false,
                        'Sat' => false,
                        'Sun' => false,
                    ]
                ]),
                new \DateTime('2018-11-29')
            )
        );

        //4) already did today
        $this->assertFalse(
            $this->ExercisesStudents->checkSchedule(
                $this->ExercisesStudents->newEntity([
                    'start_date' => '2018-12-12',
                    'interval_mode' => 'day',
                    'interval' => 1,
                    'end_mode' => 'endless',
                    'repeats' => [
                        ['date' => '2018-12-12', 'success' => true, 'fail' => false],
                        ['date' => '2018-12-13', 'success' => true, 'fail' => false],
                    ]
                ]),
                new \DateTime('2018-12-13')
            )
        );

    }
}
