<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExerciseComponentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExerciseComponentsTable Test Case
 */
class ExerciseComponentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExerciseComponentsTable
     */
    public $ExerciseComponents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exercise_components',
        'app.exercises'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ExerciseComponents') ? [] : ['className' => ExerciseComponentsTable::class];
        $this->ExerciseComponents = TableRegistry::getTableLocator()->get('ExerciseComponents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExerciseComponents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
