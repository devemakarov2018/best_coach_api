<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewsComponentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewsComponentsTable Test Case
 */
class NewsComponentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewsComponentsTable
     */
    public $NewsComponents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.news_components',
        'app.news'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NewsComponents') ? [] : ['className' => NewsComponentsTable::class];
        $this->NewsComponents = TableRegistry::getTableLocator()->get('NewsComponents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewsComponents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
