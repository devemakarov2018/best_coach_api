<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExerciseCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExerciseCategoryTable Test Case
 */
class ExerciseCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExerciseCategoriesTable
     */
    public $ExerciseCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exercise_categories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ExerciseCategories') ? [] : ['className' => ExerciseCategoriesTable::class];
        $this->ExerciseCategories = TableRegistry::getTableLocator()->get('ExerciseCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExerciseCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
