<?php
use Migrations\AbstractSeed;

/**
 * ScreensSeed seed.
 */
class ScreensSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $this->insert('screens', [
            ['screen' => 'exercises', 'title' => 'Вправи'],
            ['screen' => 'news', 'title' => 'Новини'],
            ['screen' => 'theory', 'title' => 'Теорія'],
            ['screen' => 'standards', 'title' => 'Нормативи'],
        ]);
    }
}
