<?php
use Migrations\AbstractSeed;

/**
 * Grade seed.
 */
class GradeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for ($i = 1; $i<12; $i++) {
            $data[] = ['name' => "$i-A"];
            $data[] = ['name' => "$i-Б"];
            $data[] = ['name' => "$i-В"];
        }

        $this->insert('grades', $data);

    }
}
