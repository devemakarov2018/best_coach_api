<?php
use Migrations\AbstractSeed;

/**
 * Exercise seed.
 */
class ExerciseSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        for($i=1; $i<4; $i++) {
            $data[] = [
                'name' => "Вправа $i",
                'description' => "Опис до вправи $i",
            ];
        }

        $this->insert('exercises', $data);
    }
}
