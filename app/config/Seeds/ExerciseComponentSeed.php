<?php
use Migrations\AbstractSeed;

/**
 * ExerciseComponent seed.
 */
class ExerciseComponentSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = \Faker\Factory::create('uk_UA');

        for ($i=1; $i<4; $i++) {

            $textComponentsCount = $faker->numberBetween(1,3);
            while ($textComponentsCount--) {
                $data[] = [
                    'exercise_id' => $i,
                    'type' => 'text',
                    'label' => 'label',
                    'text' => 'text',
                    'image' => null,
                    'video' => null
                ];
            }

            $imageComponentsCount = $faker->numberBetween(1,3);
            while ($imageComponentsCount--) {
                $data[] = [
                    'exercise_id' => $i,
                    'type' => 'image',
                    'label' => 'label',
                    'text' => null,
                    'image' => 'image url',
                    'video' => null
                ];
            }

            $videoComponentsCount = $faker->numberBetween(1,3);
            while ($videoComponentsCount--) {
                $data[] = [
                    'exercise_id' => $i,
                    'type' => 'video',
                    'label' => 'label',
                    'text' => null,
                    'image' => null,
                    'video' => 'video url'
                ];
            }
        }

        $this->insert('exercise_components', $data);
    }
}
