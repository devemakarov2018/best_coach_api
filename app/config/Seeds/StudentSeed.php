<?php
use Migrations\AbstractSeed;

/**
 * Student seed.
 */
class StudentSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = Faker\Factory::create('uk_UA');

        for($i=0; $i<300; $i++) {

            $isMale = $faker->randomElement([0, 1]);
            $data[] = [
                'first_name' => $isMale
                    ? $faker->firstNameMale
                    : $faker->firstNameFemale,
                'middle_name' => $isMale
                    ? $faker->middleNameMale
                    : $faker->middleNameFemale,
                'last_name' => $faker->lastName,
                'grade_id' => $faker->numberBetween(1, 33),
                'medical_group_id' => $faker->randomElement([1,1,1,1,2,2,3]),
                'birthday' => $faker
                    ->dateTimeBetween('-20 years', '-10 years')
                    ->format('Y-m-d'),
                'sex' => $isMale ? 1 : 2
            ];
        }

        $this->insert('students', $data);
    }
}
