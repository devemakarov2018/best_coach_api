<?php
use Migrations\AbstractMigration;

class AddExerciseFields extends AbstractMigration
{
    public function up()
    {
        $this->table('exercises')
            ->addColumn('interval', 'integer', [
                'null' => false,
                'default' => 1
            ])
            ->addColumn('days', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('end_date', 'date', [
                'null' => true
            ])
            ->addColumn('end_repeats', 'integer', [
                'null' => true
            ])
            ->save();

        $this->table('exercises_students')
            ->addColumn('repeats_count', 'integer', [
                'null' => false,
                'default' => 0
            ])
            ->save();

        $this->table('repeats')
            ->addColumn('exercise_student_id', 'integer', [
                'null' => false,
                'default' => 0
            ])
            ->addColumn('date', 'date')
            ->addColumn('success', 'boolean')
            ->addColumn('fail', 'boolean')
            ->save();
    }

    public function down()
    {
        $this->table('exercises')
            ->removeColumn('interval')
            ->removeColumn('days')
            ->removeColumn('end_date')
            ->removeColumn('end_repeats')
            ->save();

        $this->table('exercises_students')
            ->removeColumn('repeats_count')
            ->save();

        $this->table('repeats')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
