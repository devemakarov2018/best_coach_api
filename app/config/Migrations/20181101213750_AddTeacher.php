<?php
use Migrations\AbstractMigration;

class AddTeacher extends AbstractMigration
{
    public function up()
    {
        $teacher = $this->table('teachers');
        $teacher
            ->addColumn('first_name', 'string', [
                'default' => '',
                'null' => false,
            ])
            ->addColumn('middle_name', 'string', [
                'default' => '',
                'null' => false,
            ])
            ->addColumn('last_name', 'string', [
                'default' => '',
                'null' => false,

            ])
            ->addColumn('username', 'string', [
                'null' => false
            ])
            ->addColumn('password', 'string', [
                'null' => false
            ])
            ->addIndex('username', ['unique' => true])
            ->save();
    }

    public function down()
    {
        $this->table('teachers')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
