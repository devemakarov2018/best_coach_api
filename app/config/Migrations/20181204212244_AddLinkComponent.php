<?php
use Migrations\AbstractMigration;

class AddLinkComponent extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('exercise_components')
            ->addColumn('link', 'string', [
                'null' => true,
            ])
            ->save();
    }
}
