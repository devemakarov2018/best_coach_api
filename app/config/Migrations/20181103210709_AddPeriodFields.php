<?php
use Migrations\AbstractMigration;

class AddPeriodFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('exercises_students')
            ->addColumn('start_date', 'date', [
                'null' => false,
                'default' => 'now()'
            ])
            ->addColumn('interval_mode', 'string', [
                'null' => true
            ])
            ->addColumn('interval', 'integer', [
                'null' => false,
                'default' => 1
            ])
            ->addColumn('end_mode', 'string', [
                'null' => true
            ])
            ->addColumn('end_date', 'date', [
                'null' => true
            ])
            ->addColumn('end_repeats', 'integer', [
                'null' => true
            ])
            ->addColumn('repeats', 'json', [
                'null' => false,
                'default' => '[]'
            ])
            ->addColumn('days_of_week', 'json', [
                'null' => false,
                'default' => '{}'
            ])
            ->save();
    }
}
