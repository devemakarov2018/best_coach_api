<?php
use Migrations\AbstractMigration;

class AddPinForStudents extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('students')
            ->addColumn('pin', 'string', [
                'null' => false,
                'default' => '0000'
            ])
            ->save();
    }
}
