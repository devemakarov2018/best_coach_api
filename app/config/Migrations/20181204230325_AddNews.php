<?php
use Migrations\AbstractMigration;

class AddNews extends AbstractMigration
{

    public function up()
    {
        $news = $this->table('news');
        $news
            ->addColumn('name', 'string', [
                'null' => false
            ])
            ->addColumn('description', 'string', [
                'null' => false
            ])
            ->save();

        $newComponents = $this->table('news_components');
        $newComponents
            ->addColumn('type', 'string')
            ->addColumn('label', 'string')
            ->addColumn('text', 'string', [
                'null' => true
            ])
            ->addColumn('image', 'string', [
                'null' => true
            ])
            ->addColumn('video', 'string', [
                'null' => true
            ])
            ->addColumn('link', 'string', [
                'null' => true,
            ])
            ->addColumn('new_id', 'integer', [
                'null' => false,
                'default' => 0
            ])
            ->save();
    }

    public function down()
    {
        $this->table('news')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('news_components')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
