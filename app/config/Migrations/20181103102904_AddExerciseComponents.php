<?php
use Migrations\AbstractMigration;

class AddExerciseComponents extends AbstractMigration
{
    public function up()
    {
        $this->table('exercises')
            ->addColumn('description', 'string', [
                'after' => 'name'
            ])
            ->save();

        $this->table('exercise_components')
            ->addColumn('type', 'string')
            ->addColumn('label', 'string')
            ->addColumn('text', 'string', [
                'null' => true
            ])
            ->addColumn('image', 'string', [
                'null' => true
            ])
            ->addColumn('video', 'string', [
                'null' => true
            ])
            ->save();
    }

    public function down()
    {
        $this->table('exercises')
            ->removeColumn('description')
            ->save();

        $this->table('exercise_components')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
