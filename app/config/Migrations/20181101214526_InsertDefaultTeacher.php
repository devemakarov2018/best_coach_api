<?php

use Migrations\AbstractMigration;
use Cake\Auth\DefaultPasswordHasher;

class InsertDefaultTeacher extends AbstractMigration
{
    public function up()
    {
        $hasher = new DefaultPasswordHasher();
        $data = [
            'first_name' => 'Teacher',
            'middle_name' => 'Teacher',
            'last_name' => 'Teacher',
            'username' => 'teacher',
            'password' => $hasher->hash('123456')
        ];

        $this->table('teachers')
            ->insert($data)
            ->saveData();
    }

    public function down()
    {
        $this->table('teachers')->truncate();
    }
}
