<?php
use Migrations\AbstractMigration;

class AddTheory extends AbstractMigration
{
    public function up()
    {
        $theory = $this->table('theory_details');
        $theory
            ->addColumn('name', 'string', [
                'null' => false
            ])
            ->addColumn('description', 'string', [
                'null' => false
            ])
            ->save();

        $theoryComponents = $this->table('theory_details_components');
        $theoryComponents
            ->addColumn('type', 'string')
            ->addColumn('label', 'string')
            ->addColumn('text', 'string', [
                'null' => true
            ])
            ->addColumn('image', 'string', [
                'null' => true
            ])
            ->addColumn('video', 'string', [
                'null' => true
            ])
            ->addColumn('link', 'string', [
                'null' => true,
            ])
            ->addColumn('theory_detail_id', 'integer', [
                'null' => false,
                'default' => 0
            ])
            ->save();

        $theoryCategories = $this->table('theory_details_categories');
        $theoryCategories
            ->addColumn('name', 'string')
            ->save();

        $theoryTheoryCategories = $this->table('theory_details_theory_details_categories');
        $theoryTheoryCategories
            ->addColumn('theory_detail_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->addColumn('theory_details_category_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->save();

        $theoryStudents = $this->table('theory_details_students');
        $theoryStudents
            ->addColumn('theory_detail_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->addColumn('student_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->save();
    }

    public function down()
    {
        $this->table('theory_details')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('theory_details_components')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('theory_details_categories')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('theory_details_categories_theory_details')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('theory_details_students')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
