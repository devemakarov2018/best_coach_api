<?php
use Migrations\AbstractMigration;

class AddScreens extends AbstractMigration
{
    public function up()
    {
        $theory = $this->table('screens');
        $theory
            ->addColumn('screen', 'string', [
                'null' => false
            ])
            ->addColumn('title', 'string', [
                'null' => false
            ])
            ->addColumn('is_show', 'boolean', [
                'null' => false,
                'default' => true
            ])
            ->save();
    }

    public function down()
    {
        $this->table('screens')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
