<?php
use Migrations\AbstractMigration;

class ExerciseAndExerciseComponentRelation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('exercise_components')
            ->addColumn('exercise_id', 'integer', [
                'after' => 'id',
                'null' => false,
                'default' => 0
            ])
            ->save();
    }
}
