<?php
use Migrations\AbstractMigration;

class InsertMedicalGroups extends AbstractMigration
{
    public function up()
    {
        $this->table('medical_groups')
            ->insert(['id' => 1, 'name' => 'Основна'])
            ->insert(['id' => 2, 'name' => 'Підготовча'])
            ->insert(['id' => 3, 'name' => 'Спеціальна'])
            ->saveData();
    }

    public function down()
    {
        $this->table('medical_groups')
            ->truncate();
    }
}
