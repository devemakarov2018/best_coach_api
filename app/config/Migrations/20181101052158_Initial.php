<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {
        $grade = $this->table('grades');
        $grade
            ->addColumn('name', 'string')
            ->save();

        $student = $this->table('students');
        $student
            ->addColumn('first_name', 'string', [
                'default' => '',
                'null' => false,
            ])
            ->addColumn('middle_name', 'string', [
                'default' => '',
                'null' => false,
            ])
            ->addColumn('last_name', 'string', [
                'default' => '',
                'null' => false,

            ])
            ->addColumn('grade_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->addColumn('medical_group_id', 'integer', [
                'default' => 1,
                'null' => false,
            ])
            ->addColumn('birthday', 'date')
            ->addColumn('sex', 'integer', [
                'default' => 1,
                'null' => false
            ])
            ->save();

        $medicalGroup = $this->table('medical_groups');
        $medicalGroup
            ->addColumn('name', 'string')
            ->save();

        $exercise = $this->table('exercises');
        $exercise
            ->addColumn('name', 'string', [
                'null' => false,
            ])
            ->save();

        $exercisesStudents = $this->table('exercises_students');
        $exercisesStudents
            ->addColumn('exercise_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->addColumn('student_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->save();
    }

    public function down()
    {
        $this->table('grades')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('students')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('medical_groups')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('exercises')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('exercises_students')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
