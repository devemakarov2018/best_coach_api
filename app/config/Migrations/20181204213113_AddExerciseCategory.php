<?php
use Migrations\AbstractMigration;

class AddExerciseCategory extends AbstractMigration
{
    public function up()
    {
        $category = $this->table('exercise_categories');
        $category
            ->addColumn('name', 'string')
            ->save();

        $categoryExercise = $this->table('exercises_exercise_categories');
        $categoryExercise
            ->addColumn('exercise_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->addColumn('exercise_category_id', 'integer', [
                'default' => 0,
                'null' => false,
            ])
            ->save();
    }

    public function down()
    {
        $this->table('exercise_categories')
            ->removeIndex(['id'])
            ->drop()
            ->save();

        $this->table('exercises_exercise_categories')
            ->removeIndex(['id'])
            ->drop()
            ->save();
    }
}
