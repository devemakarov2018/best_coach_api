<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TheoryDetail Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property \App\Model\Entity\TheoryDetailsComponent[] $theory_details_components
 * @property \App\Model\Entity\Student[] $students
 * @property \App\Model\Entity\TheoryDetailsCategory[] $theory_details_categories
 */
class TheoryDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'theory_details_components' => true,
        'students' => true,
        //'theory_details_categories' => true
    ];
}
