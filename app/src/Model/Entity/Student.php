<?php
namespace App\Model\Entity;

use App\Auth\PinHasher;
use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property int $grade_id
 * @property int $medical_group_id
 * @property \Cake\I18n\FrozenDate $birthday
 * @property int $sex
 *
 * @property \App\Model\Entity\Grade $grade
 * @property \App\Model\Entity\MedicalGroup $medical_group
 * @property \App\Model\Entity\Exercise[] $exercises
 */
class Student extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'grade_id' => true,
        'medical_group_id' => true,
        'birthday' => true,
        'sex' => true,
        'grade' => true,
        'pin' => true,
        'medical_group' => true,
        'exercises' => true
    ];

    protected $_virtual = ['name'];

    protected function _setpassword($value) {
        $hasher = new PinHasher();
        return $hasher->hash($value);
    }

    protected function _getName()
    {
        $firstName = $this->_properties['first_name'];
        $middleName = $this->_properties['middle_name'];
        $lastName = $this->_properties['last_name'];

        return "$lastName $firstName $middleName";
    }
}
