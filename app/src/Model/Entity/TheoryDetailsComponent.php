<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TheoryDetailsComponent Entity
 *
 * @property int $id
 * @property string $type
 * @property string $label
 * @property string $text
 * @property string $image
 * @property string $video
 * @property string $link
 * @property int $theory_detail_id
 *
 * @property \App\Model\Entity\TheoryDetail $theory_detail
 */
class TheoryDetailsComponent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'label' => true,
        'text' => true,
        'image' => true,
        'video' => true,
        'link' => true,
        'theory_detail_id' => true,
        'theory_detail' => true
    ];
}
