<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExercisesStudent Entity
 *
 * @property int $id
 * @property int $exercise_id
 * @property int $student_id
 *
 * @property \App\Model\Entity\Exercise $exercise
 * @property \App\Model\Entity\Student $student
 */
class ExercisesStudent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'exercise_id' => true,
        'student_id' => true,
        'start_date' => true,
        'interval_mode' => true,
        'interval' => true,
        'end_mode' => true,
        'end_date' => true,
        'end_repeats' => true,
        'repeats' => true,
        'days_of_week' => true,
        'exercise' => true,
        'student' => true,
    ];
}
