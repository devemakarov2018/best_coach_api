<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TheoryDetailsStudent Entity
 *
 * @property int $id
 * @property int $theory_detail_id
 * @property int $student_id
 *
 * @property \App\Model\Entity\TheoryDetail $theory_detail
 * @property \App\Model\Entity\Student $student
 */
class TheoryDetailsStudent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'theory_detail_id' => true,
        'student_id' => true,
        'theory_detail' => true,
        'student' => true
    ];
}
