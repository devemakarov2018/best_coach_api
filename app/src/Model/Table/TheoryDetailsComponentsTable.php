<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TheoryDetailsComponents Model
 *
 * @property \App\Model\Table\TheoryDetailsTable|\Cake\ORM\Association\BelongsTo $TheoryDetails
 *
 * @method \App\Model\Entity\TheoryDetailsComponent get($primaryKey, $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsComponent findOrCreate($search, callable $callback = null, $options = [])
 */
class TheoryDetailsComponentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('theory_details_components');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TheoryDetails', [
            'foreignKey' => 'theory_detail_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->scalar('label')
            ->maxLength('label', 255)
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->scalar('text')
            ->maxLength('text', 255)
            ->allowEmpty('text');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->allowEmpty('image');

        $validator
            ->scalar('video')
            ->maxLength('video', 255)
            ->allowEmpty('video');

        $validator
            ->scalar('link')
            ->maxLength('link', 255)
            ->allowEmpty('link');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['theory_detail_id'], 'TheoryDetails'));

        return $rules;
    }
}
