<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TheoryDetailsStudents Model
 *
 * @property \App\Model\Table\TheoryDetailsTable|\Cake\ORM\Association\BelongsTo $TheoryDetails
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\BelongsTo $Students
 *
 * @method \App\Model\Entity\TheoryDetailsStudent get($primaryKey, $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsStudent findOrCreate($search, callable $callback = null, $options = [])
 */
class TheoryDetailsStudentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('theory_details_students');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TheoryDetails', [
            'foreignKey' => 'theory_detail_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Students', [
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['theory_detail_id'], 'TheoryDetails'));
        $rules->add($rules->existsIn(['student_id'], 'Students'));

        return $rules;
    }
}
