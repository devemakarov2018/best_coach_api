<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Exercises Model
 *
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\BelongsToMany $Students
 *
 * @method \App\Model\Entity\Exercise get($primaryKey, $options = [])
 * @method \App\Model\Entity\Exercise newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Exercise[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Exercise|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Exercise|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Exercise patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Exercise[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Exercise findOrCreate($search, callable $callback = null, $options = [])
 */
class ExercisesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('exercises');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Students', [
            'foreignKey' => 'exercise_id',
            'targetForeignKey' => 'student_id',
            'joinTable' => 'exercises_students'
        ]);

        $this->hasMany('ExerciseComponents', [
            'foreignKey' => 'exercise_id',
            'depend' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsToMany('ExerciseCategories', [
            'foreignKey' => 'exercise_id',
            'targetForeignKey' => 'exercise_category_id',
            'joinTable' => 'exercises_exercise_categories'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

//        $validator
//            ->integer('internal')
//            ->requirePresence('interval', 'create');
//
//        $validator
//            ->requirePresence('days', 'create')
//            ->add('days', 'validDaysJson', [
//                'rule' => 'isValidDaysJson',
//                'message' => 'Invalid days',
//                'provider' => 'table'
//            ]);

        return $validator;
    }

    public function isValidDaysJson($value) {

        $daysOfWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

        if (!is_array($value)) {
            return false;
        }

        if (!empty(array_diff($daysOfWeek, array_keys($value)))) {
            return false;
        }

        return !empty(
            array_filter(
                $value,
                function ($isDayPresent) {return boolval($isDayPresent);}
            )
        );
    }
}
