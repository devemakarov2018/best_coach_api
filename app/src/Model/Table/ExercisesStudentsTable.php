<?php
namespace App\Model\Table;

use App\Model\Entity\ExercisesStudent;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExercisesStudents Model
 *
 * @property \App\Model\Table\ExercisesTable|\Cake\ORM\Association\BelongsTo $Exercises
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\BelongsTo $Students
 *
 * @method \App\Model\Entity\ExercisesStudent get($primaryKey, $options = [])
 * @method \App\Model\Entity\ExercisesStudent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ExercisesStudent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ExercisesStudent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExercisesStudent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExercisesStudent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ExercisesStudent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ExercisesStudent findOrCreate($search, callable $callback = null, $options = [])
 */
class ExercisesStudentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('exercises_students');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Exercises', [
            'foreignKey' => 'exercise_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Students', [
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('interval_mode')
            ->requirePresence('interval_mode', 'create')
            ->add('mode', 'checkIntervalMode', [
                'rule' => 'isValidIntervalMode',
                'message' => 'Invalid interval mode',
                'provider' => 'table'
            ]);

        $validator
            ->scalar('interval')
            ->integer('interval')
            ->greaterThan('interval', 0);

        $validator
            ->scalar('end_mode')
            ->requirePresence('end_mode', 'create')
            ->add('end_mode', 'checkEndMode', [
                'rule' => 'isValidEndMode',
                'message' => 'Invalid end mode',
                'provider' => 'table'
            ]);

        $validator
            ->scalar('end_repeats')
            ->integer('end_repeats')
            ->greaterThan('interval', 0);

        $validator
            ->add('days_of_week', 'checkDaysOfWeek', [
                'rule' => 'isValidDaysOfWeek',
                'message' => 'Invalid days',
                'provider' => 'table'
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['exercise_id'], 'Exercises'));
        $rules->add($rules->existsIn(['student_id'], 'Students'));

        return $rules;
    }

    public function isValidIntervalMode($value)
    {
        return in_array($value, ['day', 'week', 'month']);
    }

    public function isValidEndMode($value)
    {
        return in_array($value, ['date', 'repeats', 'endless']);
    }

    public function isValidDaysOfWeek($value) {

        $daysOfWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

        if (!is_array($value)) {
            return false;
        }

        if (!empty(array_diff($daysOfWeek, array_keys($value)))) {
            return false;
        }

        return !empty(
            array_filter(
                $value,
                function ($isDayPresent) {return boolval($isDayPresent);}
            )
        );
    }

    public function checkSchedule(ExercisesStudent $exercise, \DateTime $date) {

        $startDate = new \DateTime($exercise->start_date);

        if ($exercise->end_mode == 'date') {

            $endDate = new \DateTime($exercise->end_date);
            if ($date->getTimestamp() > $endDate->getTimestamp()) {
                return false;
            }

        } elseif ($exercise->end_mode == 'repeats') {

            if (count($exercise->repeats) >= $exercise->end_repeats) {
                return false;
            }
        }

        if (!empty($exercise->repeats)) {
            foreach ($exercise->repeats as $repeat) {
                $repeatDate = new \DateTime($repeat['date']);
                if ($repeatDate->format('Ymd') === $date->format('Ymd')) {
                    return false;
                }
            }
        }

        if ($exercise->interval_mode == 'day') {

            $dateDiff = $date->diff($startDate);
            return $dateDiff->days % $exercise->interval == 0;

        } elseif ($exercise->interval_mode == 'month') {

            $dateDiff = $date->diff($startDate);
            return ($dateDiff->m % $exercise->interval == 0) && !$dateDiff->d;

        } elseif ($exercise->interval_mode == 'week') {

            $dayOfWeek = $date->format('D');
            $dateWeekNo = intval($date->format('W'));
            $startDateWeekNo = intval($startDate->format('W'));

            if ($date->format('Y') == $startDate->format('Y')) {
                $weekDiff = $dateWeekNo - $startDateWeekNo;
            } else {
                $startDateYear = $startDate->format('Y');
                $lastWeekNo = intval((new \DateTime("$startDateYear-12-28"))->format('W'));
                $weekDiff = $dateWeekNo + $lastWeekNo - $startDateWeekNo;
            }

            return ($weekDiff % $exercise->interval == 0) && $exercise->days_of_week[$dayOfWeek];
        }

        return false;
    }
}
