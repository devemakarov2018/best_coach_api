<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TheoryDetailsTheoryDetailsCategories Model
 *
 * @property \App\Model\Table\TheoryDetailsTable|\Cake\ORM\Association\BelongsTo $TheoryDetails
 * @property \App\Model\Table\TheoryDetailsCategoriesTable|\Cake\ORM\Association\BelongsTo $TheoryDetailsCategories
 *
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetailsTheoryDetailsCategory findOrCreate($search, callable $callback = null, $options = [])
 */
class TheoryDetailsTheoryDetailsCategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('theory_details_theory_details_categories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TheoryDetails', [
            'foreignKey' => 'theory_detail_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TheoryDetailsCategories', [
            'foreignKey' => 'theory_details_category_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['theory_detail_id'], 'TheoryDetails'));
        $rules->add($rules->existsIn(['theory_details_category_id'], 'TheoryDetailsCategories'));

        return $rules;
    }
}
