<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExerciseCategory Model
 *
 * @property \App\Model\Table\ExercisesExerciseCategoriesTable|\Cake\ORM\Association\HasMany $ExercisesExerciseCategories
 *
 * @method \App\Model\Entity\ExerciseCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\ExerciseCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ExerciseCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ExerciseCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExerciseCategory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExerciseCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ExerciseCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ExerciseCategory findOrCreate($search, callable $callback = null, $options = [])
 */
class ExerciseCategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('exercise_categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
