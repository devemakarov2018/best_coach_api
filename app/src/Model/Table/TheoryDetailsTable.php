<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TheoryDetails Model
 *
 * @property \App\Model\Table\TheoryDetailsComponentsTable|\Cake\ORM\Association\HasMany $TheoryDetailsComponents
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\BelongsToMany $Students
 * @property \App\Model\Table\TheoryDetailsCategoriesTable|\Cake\ORM\Association\BelongsToMany $TheoryDetailsCategories
 *
 * @method \App\Model\Entity\TheoryDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\TheoryDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TheoryDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TheoryDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TheoryDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class TheoryDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('theory_details');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('TheoryDetailsComponents', [
            'foreignKey' => 'theory_detail_id'
        ]);
        $this->belongsToMany('Students', [
            'foreignKey' => 'theory_detail_id',
            'targetForeignKey' => 'student_id',
            'joinTable' => 'theory_details_students'
        ]);
        $this->belongsToMany('TheoryDetailsCategories', [
            'foreignKey' => 'theory_detail_id',
            'targetForeignKey' => 'theory_details_category_id',
            'joinTable' => 'theory_details_theory_details_categories'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        return $validator;
    }
}
