<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Screens Model
 *
 * @method \App\Model\Entity\Screen get($primaryKey, $options = [])
 * @method \App\Model\Entity\Screen newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Screen[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Screen|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Screen|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Screen patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Screen[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Screen findOrCreate($search, callable $callback = null, $options = [])
 */
class ScreensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('screens');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('screen')
            ->maxLength('screen', 255)
            ->requirePresence('screen', 'create')
            ->notEmpty('screen');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }
}
