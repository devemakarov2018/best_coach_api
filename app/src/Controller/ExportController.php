<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\ExercisesStudentsTable;
use App\Model\Table\StudentsTable;
use Cake\ORM\TableRegistry;

/**
 * Export Controller
 *
 *
 */
class ExportController extends AppController
{

    /**
     * @var StudentsTable
     */
    private $students = null;

    /**
     * @var ExercisesStudentsTable
     */
    private $exercisesStudents = null;

    public function initialize()
    {
        parent::initialize();
        $this->students = TableRegistry::getTableLocator()->get('Students');
        $this->exercisesStudents = TableRegistry::getTableLocator()->get('ExercisesStudents');
    }


    public function exercises($studentId = 0, $mode = null) {

        $this->request->allowMethod(['get', 'options']);

        if (empty($studentId) || empty($mode) || !in_array($mode, ['week', 'month'])) {
            return $this->response->withStatus(400);
        }

        $student = $this->students
            ->find()
            ->where(['Students.id' => $studentId])
            ->first();

        if (!$student) {
            return $this->response->withStatus(404);
        }

        $exercises = $this->exercisesStudents
            ->find()
            ->contain([
                'Exercises',
                'Exercises.ExerciseCategories',
                'Students'
            ])
            ->where(['student_id' => $studentId])
            ->all();

        $startDate = new \DateTime();
        $endDate = new \DateTime();
        $endDate->add(
            new \DateInterval('P1' . ($mode === 'week' ? 'W' : 'M'))
        );

        $export = [];
        while ($startDate->format('Ymd') != $endDate->format('Ymd')) {
            $date = $startDate->format('Y-m-d');
            foreach ($exercises as $exercise) {
                if ($this->exercisesStudents->checkSchedule($exercise, $startDate)) {
                    if (empty($export[$date])) {
                        $export[$date] = [];
                    }
                    $categories = array_map(function($category) {
                        return $category->name;
                    }, $exercise->exercise->exercise_categories);
                    $export[$date][] = $exercise->exercise->name . (
                        !empty($categories)
                            ? ' (' . implode(', ', $categories) . ')'
                            : ''
                        );
                }
            }
            $startDate->add(new \DateInterval('P1D'));
        }

        if (empty($export)) {
            return $this->response->withStatus(404);
        }

        $string = 'Вправи на наступний ' . ($mode == 'week' ? 'тиждень:' : 'місяць:') . "\n\n";
        foreach ($export as $date => $exercises) {
            $string .= "$date\n";
            foreach ($exercises as $exercise) {
                $string .= "- $exercise\n";
            }
            $string .= "\n";
        }

        $response = $this->response;
        $response = $response->withStringBody($string);
        $response = $response->withType('txt');
        $response = $response->withDownload('exercises.txt');
        return $response;
    }
}
