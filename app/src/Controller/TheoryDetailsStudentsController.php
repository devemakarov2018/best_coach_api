<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\TheoryDetailsStudentsTable;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * TheoryDetailsStudents Controller
 *
 * @property \App\Model\Table\TheoryDetailsStudentsTable $TheoryDetailsStudents
 *
 * @method \App\Model\Entity\TheoryDetailsStudent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TheoryDetailsStudentsController extends ApiController
{
    /**
     * @var TheoryDetailsStudentsTable
     */
    private $theoryDetailsStudents = null;

    public function initialize()
    {
        parent::initialize();
        $this->theoryDetailsStudents = TableRegistry::getTableLocator()->get('TheoryDetailsStudents');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        $students = $this->request->getData('students') ?? null;
        $theoryDetails = $this->request->getData('theoryDetails') ?? null;

        if (!$students || !$theoryDetails) {
            return $this->response->withStatus(400);
        }

        foreach ($students as $studentId) {
            foreach ($theoryDetails as $theoryDetailId) {

                $isRelationAlreadyExists = $this->theoryDetailsStudents
                    ->find()
                    ->where([
                        'student_id' => $studentId,
                        'theory_detail_id' => $theoryDetailId
                    ])
                    ->first();

                if ($isRelationAlreadyExists) {
                    continue;
                }

                $entity = $this->theoryDetailsStudents->newEntity();
                $entity->student_id = $studentId;
                $entity->theory_detail_id = $theoryDetailId;

                $this->theoryDetailsStudents->save($entity);
            }
        }
    }

    /**
     * Delete method
     *
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['delete']);

        $students = $this->request->getData('students') ?? null;
        $theoryDetails = $this->request->getData('theoryDetails') ?? null;

        if (!$students || !$theoryDetails) {
            return $this->response->withStatus(400);
        }

        foreach ($students as $studentId) {
            foreach ($theoryDetails as $theoryDetailId) {

                $relation = $this->theoryDetailsStudents
                    ->find()
                    ->where([
                        'student_id' => $studentId,
                        'theory_detail_id' => $theoryDetailId
                    ])
                    ->first();

                if (!$relation) {
                    continue;
                }

                $this->theoryDetailsStudents->delete($relation);
            }
        }
    }

    public function index()
    {
        $this->request->allowMethod(['post']);

        $studentId = $this->request->getData('student_id');
        if (!$studentId) {
            return $this->response->withStatus(400);
        }

        $theoryDetails = $this->theoryDetailsStudents
            ->find()
            ->contain([
                'TheoryDetails',
                'TheoryDetails.TheoryDetailsComponents',
                'Students'
            ])
            ->where(['student_id' => $studentId])
            ->all();

        $this->apiResponse['theoryDetails'] = $theoryDetails;
    }
}
