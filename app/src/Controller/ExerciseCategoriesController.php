<?php
namespace App\Controller;

use App\Model\Table\ExerciseCategoriesTable;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * ExerciseCategory Controller
 *
 * @property \App\Model\Table\ExerciseCategoriesTable $ExerciseCategory
 *
 * @method \App\Model\Entity\ExerciseCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExerciseCategoriesController extends ApiController
{
    /**
     * @var ExerciseCategoriesTable
     */
    private $exerciseCategories;

    public function initialize()
    {
        parent::initialize();
        $this->exerciseCategories = TableRegistry::getTableLocator()->get('ExerciseCategories');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod('get');
        $categories = $this->exerciseCategories
            ->find()
            ->all();

        $this->apiResponse['categories'] = $categories;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        if (!$this->request->getData('name')) {
            return $this->response->withStatus(400);
        }

        $category = $this->exerciseCategories->newEntity(
            $this->request->getData()
        );
        $errors = $category->getErrors();

        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $this->exerciseCategories->save($category);

        $category = $this->exerciseCategories
            ->find()
            ->where(['ExerciseCategories.id' => $category->id])
            ->first();

        $this->apiResponse['category'] = $category->toArray();
    }
}
