<?php
namespace App\Controller;

use App\Model\Table\GradesTable;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * Grades Controller
 *
 *
 * @method \App\Model\Entity\Grade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GradesController extends ApiController
{
    /**
     * @var GradesTable
     */
    private $grades = null;

    public function initialize()
    {
        parent::initialize();
        $this->grades = TableRegistry::getTableLocator()->get('Grades');
    }

    public function index()
    {
        $this->request->allowMethod(['get']);

        $query = $this->grades->find();
        $grades = $query->toArray();
        $this->apiResponse['grades'] = $grades;
    }

    public function view($id = null)
    {
        $this->request->allowMethod('get');
        $query = $this->grades->find();
        $grade = $query
            ->where(['id' => $id])
            ->first();

        if (!$grade) {
            return $this->response->withStatus(404);
        }

        $this->apiResponse['grade'] = $grade->toArray();
    }

    public function add()
    {
        $this->request->allowMethod(['post']);

        $grade = $this->grades->newEntity($this->request->getData());
        $errors = $grade->getErrors();
        if($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $this->grades->save($grade);
        $this->apiResponse['grade'] = $grade->toArray();
    }

    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        $grade = $this->grades
            ->find()
            ->where(['id' => $id])
            ->first();

        if (empty($grade)) {
            return $this->response->withStatus(404);
        }

        $grade->set($this->request->getData());
        if ($grade->getErrors()) {
            return $this->response->withStatus(400);
        }

        $this->grades->save($grade);
        $this->apiResponse['grade'] = $grade->toArray();
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $grade = $this->grades
            ->find()
            ->where(['id' => $id])
            ->first();

        if (empty($grade)) {
            return $this->response->withStatus(404);
        }

        $this->grades->delete($grade);
        $this->apiResponse['grade'] = $grade->toArray();
    }
}
