<?php
namespace App\Controller;

use App\Model\Table\ExerciseCategoriesTable;
use App\Model\Table\ExerciseComponentsTable;
use App\Model\Table\ExercisesTable;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * Exercises Controller
 *
 *
 * @method \App\Model\Entity\Exercise[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExercisesController extends ApiController
{

    /**
     * @var ExercisesTable
     */
    private $exercises = null;

    /**
     * @var ExerciseComponentsTable
     */
    private $exerciseComponents = null;

    /**
     * @var ExerciseCategoriesTable
     */
    private $exerciseCategories = null;

    public function initialize()
    {
        parent::initialize();
        $this->exercises = TableRegistry::getTableLocator()->get('Exercises');
        $this->exerciseComponents = TableRegistry::getTableLocator()->get('ExerciseComponents');
        $this->exerciseCategories = TableRegistry::getTableLocator()->get('ExerciseCategories');
    }

    public function index()
    {
        $this->request->allowMethod(['get']);

        $exercises = $this->exercises
            ->find()
            ->contain(['ExerciseComponents', 'ExerciseCategories'])
            ->all();

        $this->apiResponse['exercises'] = $exercises->toArray();
    }

    public function view($id = null)
    {
        $this->request->allowMethod(['get']);

        $exercise = $this->exercises
            ->find()
            ->contain([
                'ExerciseComponents',
                'Students',
                'Students.Grades',
                'Students.MedicalGroups',
                'ExerciseCategories'
            ])
            ->where(['Exercises.id' => $id])
            ->first();

        if (!$exercise) {
            return $this->response->withStatus(404);
        }

        $this->apiResponse['exercise'] = $exercise->toArray();
    }

    public function add()
    {
        $this->request->allowMethod(['post']);

        if (!$this->request->getData('Exercise')) {
            return $this->response->withStatus(400);
        }

        $exercise = $this->exercises->newEntity($this->request->getData('Exercise'));
        $errors = $exercise->getErrors();

        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        if (!$this->request->getData('ExerciseComponents')) {
            return $this->response->withStatus(400);
        }

        $components = $this->exerciseComponents->newEntities(
            $this->request->getData('ExerciseComponents')
        );

        foreach ($components as $component) {
            $errors = $component->getErrors();
            if ($errors) {
                return $this->response
                    ->withStringBody(json_encode($errors))
                    ->withStatus(400);
            }
        }

        $categories = $this->request->getData('ExerciseCategories');
        if ($categories) {
            $exerciseCategories = $this->exerciseCategories
                ->find()
                ->where(['ExerciseCategories.id IN' => $categories])
                ->all()
                ->toList();
        } else {
            $exerciseCategories = [];
        }

        $exercise->exercise_components = $components;
        $exercise->exercise_categories = $exerciseCategories;
        $this->exercises->save($exercise);

        $exercise = $this->exercises
            ->find()
            ->contain([
                'ExerciseComponents', 'ExerciseCategories'
            ])
            ->where(['Exercises.id' => $exercise->id])
            ->first();

        $this->apiResponse['exercise'] = $exercise->toArray();
    }

    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        if (!$this->request->getData('Exercise')) {
            return $this->response->withStatus(400);
        }
        if (!$this->request->getData('ExerciseComponents')) {
            return $this->response->withStatus(400);
        }

        $exercise = $this->exercises
            ->find()
            ->where(['Exercises.id' => $id])
            ->first();

        if (!$exercise) {
            return $this->response->withStatus(404);
        }

        $exercise = $this->exercises->patchEntity(
            $exercise,
            $this->request->getData('Exercise')
        );
        $errors = $exercise->getErrors();
        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $components = $this->exerciseComponents->newEntities(
            $this->request->getData('ExerciseComponents')
        );
        foreach ($components as $component) {
            $errors = $component->getErrors();
            if ($errors) {
                return $this->response
                    ->withStringBody(json_encode($errors))
                    ->withStatus(400);
            }
        }

        $categories = $this->request->getData('ExerciseCategories');
        if ($categories) {
            $exerciseCategories = $this->exerciseCategories
                ->find()
                ->where(['ExerciseCategories.id IN' => $categories])
                ->all()
                ->toList();
        } else {
            $exerciseCategories = [];
        }

        $this->exerciseComponents->deleteAll(['exercise_id' => $id]);
        $exercise->exercise_components = $components;
        $exercise->exercise_categories = $exerciseCategories;
        $this->exercises->save($exercise);

        $exercise = $this->exercises
            ->find()
            ->contain([
                'ExerciseComponents', 'ExerciseCategories'
            ])
            ->where(['Exercises.id' => $id])
            ->first();

        $this->apiResponse['exercise'] = $exercise->toArray();
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $exercise = $this->exercises
            ->find()
            ->contain([
                'ExerciseComponents', 'ExerciseCategories'
            ])
            ->where(['Exercises.id' => $id])
            ->first();

        if (!$exercise) {
            return $this->response->withStatus(404);
        }

        $this->exerciseComponents->deleteAll(['exercise_id' => $id]);
        $this->exercises->delete($exercise);

        $this->apiResponse['exercise'] = $exercise->toArray();
    }
}
