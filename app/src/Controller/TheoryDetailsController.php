<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\TheoryDetailsCategoriesTable;
use App\Model\Table\TheoryDetailsComponentsTable;
use App\Model\Table\TheoryDetailsTable;
use App\Model\Table\TheoryDetailsTheoryDetailsCategoriesTable;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * TheoryDetails Controller
 *
 * @property \App\Model\Table\TheoryDetailsTable $TheoryDetails
 *
 * @method \App\Model\Entity\TheoryDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TheoryDetailsController extends ApiController
{
    /**
     * @var TheoryDetailsTable
     */
    private $theoryDetails = null;

    /**
     * @var TheoryDetailsComponentsTable
     */
    private $theoryDetailsComponents = null;

    /**
     * @var TheoryDetailsCategoriesTable
     */
    private $theoryDetailsCategories = null;

    /**
     * @var TheoryDetailsTheoryDetailsCategoriesTable
     */
    private $theoryManyToManyCategory = null;

    public function initialize()
    {
        parent::initialize();
        $this->theoryDetails = TableRegistry::getTableLocator()->get('TheoryDetails');
        $this->theoryDetailsComponents = TableRegistry::getTableLocator()->get('TheoryDetailsComponents');
        $this->theoryDetailsCategories = TableRegistry::getTableLocator()->get('TheoryDetailsCategories');
        $this->theoryManyToManyCategory = TableRegistry::getTableLocator()->get('TheoryDetailsTheoryDetailsCategories');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $theoryDetails = $this->theoryDetails
            ->find()
            ->contain([
                'TheoryDetailsComponents', 'TheoryDetailsCategories'
            ])
            ->all();

        $this->apiResponse['theoryDetails'] = $theoryDetails->toArray();
    }

    /**
     * View method
     *
     * @param string|null $id Theory Detail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->request->allowMethod(['get']);

        $theoryDetail = $this->theoryDetails
            ->find()
            ->contain([
                'TheoryDetailsComponents',
                'TheoryDetailsCategories',
                'Students',
                'Students.Grades',
                'Students.MedicalGroups',
            ])
            ->where(['TheoryDetails.id' => $id])
            ->first();

        $this->apiResponse['theoryDetail'] = $theoryDetail->toArray();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        if (!$this->request->getData('TheoryDetails')) {
            return $this->response->withStatus(400);
        }

        $theoryDetail = $this->theoryDetails->newEntity($this->request->getData('TheoryDetails'));
        $errors = $theoryDetail->getErrors();

        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        if (!$this->request->getData('TheoryDetailsComponents')) {
            return $this->response->withStatus(400);
        }

        $components = $this->theoryDetailsComponents->newEntities(
            $this->request->getData('TheoryDetailsComponents')
        );

        foreach ($components as $component) {
            $errors = $component->getErrors();
            if ($errors) {
                return $this->response
                    ->withStringBody(json_encode($errors))
                    ->withStatus(400);
            }
        }

        $categories = $this->request->getData('TheoryDetailsCategories');
        if ($categories) {
            $theoryCategories = $this->theoryDetailsCategories
                ->find()
                ->where(['TheoryDetailsCategories.id IN' => $categories])
                ->all()
                ->toList();
        } else {
            $theoryCategories = [];
        }

        $theoryDetail->theory_details_components = $components;
        $theoryDetail->theory_details_categories = $theoryCategories;
        $this->theoryDetails->save($theoryDetail);

        $theoryDetail = $this->theoryDetails
            ->find()
            ->contain([
                'TheoryDetailsComponents', 'TheoryDetailsCategories'
            ])
            ->where(['TheoryDetails.id' => $theoryDetail->id])
            ->first();

        $this->apiResponse['theoryDetail'] = $theoryDetail->toArray();
    }

    /**
     * Edit method
     *
     * @param string|null $id Theory Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        if (!$this->request->getData('TheoryDetails')) {
            return $this->response->withStatus(400);
        }
        if (!$this->request->getData('TheoryDetailsComponents')) {
            return $this->response->withStatus(400);
        }

        $theoryDetail = $this->theoryDetails
            ->find()
            ->contain([
                'TheoryDetailsComponents', 'TheoryDetailsCategories'
            ])
            ->where(['TheoryDetails.id' => $id])
            ->first();

        if (!$theoryDetail) {
            return $this->response->withStatus(404);
        }

        $theoryDetail = $this->theoryDetails->patchEntity(
            $theoryDetail,
            $this->request->getData('TheoryDetails')
        );
        $errors = $theoryDetail->getErrors();
        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $components = $this->theoryDetailsComponents->newEntities(
            $this->request->getData('TheoryDetailsComponents')
        );
        foreach ($components as $component) {
            $errors = $component->getErrors();
            if ($errors) {
                return $this->response
                    ->withStringBody(json_encode($errors))
                    ->withStatus(400);
            }
        }


        $this->theoryDetailsComponents->deleteAll(['theory_detail_id' => $id]);
        $theoryDetail->theory_details_components = $components;
        $this->theoryDetails->save($theoryDetail);

        $categories = $this->request->getData('TheoryDetailsCategories');

        $this->theoryManyToManyCategory->deleteAll(['theory_detail_id' => $id]);
        $entities = [];
        foreach ($categories as $category) {
            $entities[] = $this->theoryManyToManyCategory->newEntity([
                'theory_detail_id' => $id,
                'theory_details_category_id' => $category
            ]);
        }
        $this->theoryManyToManyCategory->saveMany($entities);

        $theoryDetail = $this->theoryDetails
            ->find()
            ->contain([
                'TheoryDetailsComponents', 'TheoryDetailsCategories'
            ])
            ->where(['TheoryDetails.id' => $id])
            ->first();

        $this->apiResponse['theoryDetail'] = $theoryDetail->toArray();
    }

    /**
     * Delete method
     *
     * @param string|null $id Theory Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $theoryDetail = $this->theoryDetails
            ->find()
            ->contain([
                'TheoryDetailsComponents', 'TheoryDetailsCategories'
            ])
            ->where(['TheoryDetails.id' => $id])
            ->first();

        if (!$theoryDetail) {
            return $this->response->withStatus(404);
        }

        $this->theoryDetailsComponents->deleteAll(['theory_detail_id' => $id]);
        $this->theoryDetails->delete($theoryDetail);

        $this->apiResponse['theoryDetail'] = $theoryDetail->toArray();
    }
}
