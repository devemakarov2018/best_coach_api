<?php
namespace App\Controller;

use Cake\Event\Event;
use RestApi\Controller\ApiController;

/**
 * Images Controller
 *
 *
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImagesController extends ApiController
{
    public function beforeRender(Event $event)
    {
        $this->response->type('json');
        return parent::beforeRender($event);
    }

    public function upload()
    {
        $this->request->allowMethod(['post']);
        $mimes = new \Mimey\MimeTypes();

        $image = $this->request->getUploadedFile('image');
        if (!$image) {
            return $this->response->withStatus(400);
        }
        $extension =$mimes->getExtension($image->getClientMediaType());
        $filename = \Cake\Utility\Security::randomString(5) . ".$extension";
        $path = WWW_ROOT . 'uploads' . DS . $filename;

        $image->moveTo($path);
        $this->apiResponse['path'] = $_SERVER['HTTP_HOST'] . "/uploads/$filename";
    }
}
