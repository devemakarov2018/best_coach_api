<?php
namespace App\Controller;


use App\Model\Table\ExercisesStudentsTable;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;


/**
 * ExercisesStudents Controller
 *
 *
 * @method \App\Model\Entity\ExercisesStudent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExercisesStudentsController extends ApiController
{
    /**
     * @var ExercisesStudentsTable
     */
    private $exercisesStudents = null;

    public function initialize()
    {
        parent::initialize();
        $this->exercisesStudents = TableRegistry::getTableLocator()->get('ExercisesStudents');
    }

    public function add()
    {
        $this->request->allowMethod(['post']);

        $students = $this->request->getData('students') ?? null;
        $exercises = $this->request->getData('exercises') ?? null;
        $period = $this->request->getData('period') ?? null;

        if (!$students || !$exercises || !$period) {
            return $this->response->withStatus(400);
        }

        $entity = $this->exercisesStudents->newEntity($period);
        $errors = $entity->getErrors();
        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        foreach ($students as $studentId) {
            foreach ($exercises as $exerciseId) {

                $isRelationAlreadyExists = $this->exercisesStudents
                    ->find()
                    ->where([
                        'student_id' => $studentId,
                        'exercise_id' => $exerciseId
                    ])
                    ->first();

                if ($isRelationAlreadyExists) {
                    continue;
                }

                $entity = $this->exercisesStudents->newEntity($period);
                $entity->student_id = $studentId;
                $entity->exercise_id = $exerciseId;

                $this->exercisesStudents->save($entity);
            }
        }
    }

    public function delete()
    {
        $this->request->allowMethod(['delete']);

        $students = $this->request->getData('students') ?? null;
        $exercises = $this->request->getData('exercises') ?? null;

        if (!$students || !$exercises) {
            return $this->response->withStatus(400);
        }

        foreach ($students as $studentId) {
            foreach ($exercises as $exerciseId) {

                $relation = $this->exercisesStudents
                    ->find()
                    ->where([
                        'student_id' => $studentId,
                        'exercise_id' => $exerciseId
                    ])
                    ->first();

                if (!$relation) {
                    continue;
                }

                $this->exercisesStudents->delete($relation);
            }
        }
    }

    public function schedule()
    {
        $this->request->allowMethod(['post']);

        $studentId = $this->request->getData('student_id');
        if (!$studentId) {
            return $this->response->withStatus(400);
        }

        $exercises = $this->exercisesStudents
            ->find()
            ->contain([
                'Exercises',
                'Exercises.ExerciseComponents',
                'Students'
            ])
            ->where(['student_id' => $studentId])
            ->all();

        $result = [];
        $date = new \DateTime();
        foreach ($exercises as $exercise) {
            $needShowExercise = $this->exercisesStudents->checkSchedule($exercise, $date);
            if ($needShowExercise) {
                $result[] = $exercise;
            }
        }

        $this->apiResponse['exercises'] = $result;
    }

    public function markWorkout()
    {
        $this->request->allowMethod(['post']);

        $studentId = $this->request->getData('student_id');
        $exerciseId = $this->request->getData('exercise_id');
        $repeat = $this->request->getData('repeat');

        if (!$studentId || !$exerciseId || !$repeat) {
            return $this->response->withStatus(400);
        }

        $exercise = $this->exercisesStudents
            ->find()
            ->where([
                'student_id' => $studentId,
                'exercise_id' => $exerciseId
            ])
            ->first();

        if (!$exercise) {
            return $this->response->withStatus(404);
        }

        $repeats = $exercise->repeats;
        $repeats[] = $repeat;
        $exercise->set('repeats', $repeats);
        $this->exercisesStudents->save($exercise);
        $this->apiResponse['exercise'] = $exercise;
    }
}
