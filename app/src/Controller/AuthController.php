<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Http\Exception\UnauthorizedException;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;
use RestApi\Utility\JwtToken;

/**
 * Auth Controller
 *
 *
 * @method \App\Model\Entity\Auth[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthController extends ApiController
{

    public function loginTeacher()
    {
        $this->initAuth('teacher');
        $teacher = $this->Auth->identify();

        if (empty($teacher)) {
            return $this->response->withStatus(401);
        }

        $payload = [
            'type' => 'teacher',
            'user' => $teacher
        ];
        $this->apiResponse['token'] = JwtToken::generateToken($payload);
        $this->apiResponse['user'] = $teacher;
    }

    public function loginStudent()
    {
        $this->initAuth('student');
        $student = $this->Auth->identify();

        if (empty($student)) {
            return $this->response->withStatus(401);
        }

        $payload = [
            'type' => 'student',
            'user' => $student
        ];
        $this->apiResponse['token'] = JwtToken::generateToken($payload);
        $this->apiResponse['user'] = $student;
    }

    public function initAuth($userType)
    {
        $this->request->allowMethod(['post']);

        if ($userType == 'teacher') {
            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Form' => [
                        'fields' => [
                            'username' => 'username',
                            'password' => 'password'
                        ],
                        'userModel' => 'Teachers',
                        'passwordHasher' => 'Default'
                    ]
                ],
            ]);
        } elseif ($userType == 'student') {
            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Form' => [
                        'fields' => [
                            'username' => 'id',
                            'password' => 'pin'
                        ],
                        'userModel' => 'Students',
                        'passwordHasher' => 'App\Auth\PinHasher'
                    ]
                ],
            ]);
        }
    }
}
