<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\TheoryDetailsCategoriesTable;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * TheoryDetailsCategories Controller
 *
 * @property \App\Model\Table\TheoryDetailsCategoriesTable $TheoryDetailsCategories
 *
 * @method \App\Model\Entity\TheoryDetailsCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TheoryDetailsCategoriesController extends ApiController
{

    /**
     * @var TheoryDetailsCategoriesTable
     */
    private $theoryDetailsCategories = null;

    public function initialize()
    {
        parent::initialize();
        $this->theoryDetailsCategories = TableRegistry::getTableLocator()->get('TheoryDetailsCategories');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $categories = $this->theoryDetailsCategories
            ->find()
            ->all();

        $this->apiResponse['categories'] = $categories->toArray();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        if (!$this->request->getData('name')) {
            return $this->response->withStatus(400);
        }

        $category = $this->theoryDetailsCategories->newEntity(
            $this->request->getData()
        );
        $errors = $category->getErrors();

        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $this->theoryDetailsCategories->save($category);

        $category = $this->theoryDetailsCategories
            ->find()
            ->where(['TheoryDetailsCategories.id' => $category->id])
            ->first();

        $this->apiResponse['category'] = $category->toArray();
    }
}
