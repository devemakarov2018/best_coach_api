<?php
namespace App\Controller;

use App\Model\Table\StudentsTable;
use Cake\Database\Expression\QueryExpression;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * Students Controller
 *
 *
 * @method \App\Model\Entity\Student[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StudentsController extends ApiController
{

    /**
     * @var StudentsTable
     */
    private $students = null;

    public function initialize()
    {
        parent::initialize();
        $this->students = TableRegistry::getTableLocator()->get('Students');
    }

    public function beforeRender(Event $event)
    {
        $this->response->type('json');
        return parent::beforeRender($event);
    }

    public function index()
    {
        $this->request->allowMethod(['get']);

        $filters = $this->request->getQuery();
        $query = $this->students->find();

        if (isset($filters['grade_id'])) {
            $query->andWhere([
                'Students.grade_id' => $filters['grade_id']
            ]);
        }
        if (isset($filters['medical_group_id'])) {
            $query->andWhere([
                'Students.medical_group_id' => $filters['medical_group_id']
            ]);
        }
        if (isset($filters['sex'])) {
            $query->andWhere([
                'Students.sex' => $filters['sex']
            ]);
        }
        if (isset($filters['first_name'])) {
            $query->andWhere(function (QueryExpression $exp, Query $q) use ($filters) {
                $filter = $filters['name'];
                return $exp->like('Students.first_name', "%$filter%");
            });
        }
        if (isset($filters['middle_name'])) {
            $query->andWhere(function (QueryExpression $exp, Query $q) use ($filters) {
                $filter = $filters['middle_name'];
                return $exp->like('Students.middle_name', "%$filter%");
            });
        }
        if (isset($filters['last_name'])) {
            $query->andWhere(function (QueryExpression $exp, Query $q) use ($filters) {
                $filter = $filters['last_name'];
                return $exp->like('Students.last_name', "%$filter%");
            });
        }

        $students = $query
            ->contain(['Grades', 'MedicalGroups'])
            ->all();

        $this->apiResponse['students'] = $students;
    }

    public function view($id = null)
    {
        $this->request->allowMethod(['get']);

        $student = $this->students
            ->find()
            ->where(['Students.id' => $id])
            ->contain([
                'Grades',
                'MedicalGroups',
                'Exercises',
                'Exercises.ExerciseCategories',
                'TheoryDetails',
                'TheoryDetails.TheoryDetailsCategories',
            ])
            ->first();

        if (empty($student)) {
            return $this->response->withStatus(404);
        }

        $this->apiResponse['student'] = $student;
    }

    public function add()
    {
        $this->request->allowMethod(['post']);

        $student = $this->students->newEntity($this->request->getData());
        $errors = $student->getErrors();

        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $this->students->save($student);
        $student = $this->students
            ->find()
            ->where(['Students.id' => $student->id])
            ->contain(['Grades', 'MedicalGroups'])
            ->first();

        $this->apiResponse['student'] = $student->toArray();
    }

    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        $student = $this->students
            ->find()
            ->where(['id' => $id])
            ->first();

        if (empty($student)) {
            return $this->response->withStatus(404);
        }

        $student->set($this->request->getData());
        $errors = $student->getErrors();
        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $this->students->save($student);
        $student = $this->students
            ->find()
            ->where(['Students.id' => $student->id])
            ->contain(['Grades', 'MedicalGroups'])
            ->first();

        $this->apiResponse['student'] = $student->toArray();
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $student = $this->students
            ->find()
            ->where(['Students.id' => $id])
            ->contain(['Grades', 'MedicalGroups'])
            ->first();

        if (empty($student)) {
            return $this->response->withStatus(404);
        }

        $this->students->delete($student);
        $this->apiResponse['student'] = $student->toArray();
    }

    public function changeGrade()
    {
        $this->request->allowMethod(['post']);

        $students = $this->request->getData('students') ?? null;
        $grade = $this->request->getData('grade') ?? null;

        if (!$students || !$grade) {
            return $this->response->withStatus(400);
        }

        $students = $this->students
            ->find()
            ->whereInList('id', $students)
            ->all()
            ->toList();

        foreach ($students as $student) {
            $student->grade_id = $grade;
            $this->students->save($student);
        }
    }
}
