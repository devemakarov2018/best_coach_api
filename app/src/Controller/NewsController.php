<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\NewsComponentsTable;
use App\Model\Table\NewsTable;
use Cake\ORM\TableRegistry;
use Phinx\Db\Plan\NewTable;
use RestApi\Controller\ApiController;

/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News
 *
 * @method \App\Model\Entity\News[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NewsController extends ApiController
{

    /**
     * @var NewsTable
     */
    private $news = null;

    /**
     * @var NewsComponentsTable
     */
    private $newsComponents = null;

    public function initialize()
    {
        parent::initialize();
        $this->news = TableRegistry::getTableLocator()->get('News');
        $this->newsComponents = TableRegistry::getTableLocator()->get('NewsComponents');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $news = $this->news
            ->find()
            ->contain(['NewsComponents'])
            ->all();

        $this->apiResponse['news'] = $news;
    }

    /**
     * View method
     *
     * @param string|null $id News id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->request->allowMethod(['get']);

        $news = $this->news
            ->find()
            ->contain(['NewsComponents'])
            ->where(['News.id' => $id])
            ->first();

        $this->apiResponse['news'] = $news;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        if (!$this->request->getData('News')) {
            return $this->response->withStatus(400);
        }

        $news = $this->news->newEntity($this->request->getData('News'));
        $errors = $news->getErrors();

        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        if (!$this->request->getData('NewsComponents')) {
            return $this->response->withStatus(400);
        }

        $components = $this->newsComponents->newEntities(
            $this->request->getData('NewsComponents')
        );

        foreach ($components as $component) {
            $errors = $component->getErrors();
            if ($errors) {
                return $this->response
                    ->withStringBody(json_encode($errors))
                    ->withStatus(400);
            }
        }

        $news->news_components = $components;
        $this->news->save($news);

        $news = $this->news
            ->find()
            ->contain(['NewsComponents'])
            ->where(['News.id' => $news->id])
            ->first();

        $this->apiResponse['news'] = $news;
    }

    /**
     * Edit method
     *
     * @param string|null $id News id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        if (!$this->request->getData('News')) {
            return $this->response->withStatus(400);
        }
        if (!$this->request->getData('NewsComponents')) {
            return $this->response->withStatus(400);
        }

        $news = $this->news
            ->find()
            ->where(['News.id' => $id])
            ->first();

        if (!$news) {
            return $this->response->withStatus(404);
        }

        $news = $this->news->patchEntity(
            $news,
            $this->request->getData('News')
        );
        $errors = $news->getErrors();
        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }

        $components = $this->newsComponents->newEntities(
            $this->request->getData('NewsComponents')
        );
        foreach ($components as $component) {
            $errors = $component->getErrors();
            if ($errors) {
                return $this->response
                    ->withStringBody(json_encode($errors))
                    ->withStatus(400);
            }
        }

        $this->newsComponents->deleteAll(['new_id' => $id]);
        $news->news_components = $components;
        $this->news->save($news);

        $news = $this->news
            ->find()
            ->contain(['NewsComponents'])
            ->where(['News.id' => $id])
            ->first();

        $this->apiResponse['news'] = $news->toArray();
    }

    /**
     * Delete method
     *
     * @param string|null $id News id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $news = $this->news
            ->find()
            ->contain([
                'NewsComponents'
            ])
            ->where(['News.id' => $id])
            ->first();

        if (!$news) {
            return $this->response->withStatus(404);
        }

        $this->newsComponents->deleteAll(['new_id' => $id]);
        $this->news->delete($news);

        $this->apiResponse['news'] = $news->toArray();
    }
}
