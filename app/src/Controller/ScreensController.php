<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\ScreensTable;
use Cake\ORM\TableRegistry;
use RestApi\Controller\ApiController;

/**
 * Screens Controller
 *
 * @property \App\Model\Table\ScreensTable $Screens
 *
 * @method \App\Model\Entity\Screen[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ScreensController extends ApiController
{
    /**
     * @var ScreensTable
     */
    private $screens = null;

    public function initialize()
    {
        parent::initialize();
        $this->screens = TableRegistry::getTableLocator()->get('Screens');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod('get');

        $screens = $this->screens
            ->find()
            ->all();

        $this->apiResponse['screens'] = $screens;
    }

    public function view($screenName)
    {
        $this->request->allowMethod('get');

        $screen = $this->screens
            ->find()
            ->where(['Screens.screen' => $screenName])
            ->first();

        if (!$screen) {
            return $this->response->withStatus(404);
        }

        $this->apiResponse['screen'] = $screen;
    }

    /**
     * Edit method
     *
     * @param string|null $id Screen id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod('put');

        $screen = $this->screens
            ->find()
            ->where(['Screens.id' => $id])
            ->first();

        if (!$screen) {
            return $this->response->withStatus(404);
        }

        $screen = $this->screens->patchEntity(
            $screen,
            $this->request->getData()
        );

        $errors = $screen->getErrors();
        if ($errors) {
            return $this->response
                ->withStringBody(json_encode($errors))
                ->withStatus(400);
        }
        $this->screens->save($screen);

        $this->apiResponse['screen'] = $screen;
    }
}
